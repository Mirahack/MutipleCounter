import { configureStore } from '@reduxjs/toolkit'
import counterReduce from './counterSlice'

export default configureStore({
    reducer: counterReduce
})