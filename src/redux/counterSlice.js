import { createSlice } from '@reduxjs/toolkit'

export const counterSlice = createSlice({
    name: 'counter',
    initialState: {
        counterList: []
    },
    reducers: {
        updateCounterList: (state, action) => {
            let tempList = []
            for (let index = 0; index < action.payload; index++) {
                tempList.push({id:index, value:0})
            }
            state.counterList = tempList
        },
        updateCounterValue: (state, action) => {
            const updateCounterList = [...state.counterList]
            updateCounterList[action.payload.index].value = action.payload.value
            state.counterList = updateCounterList
        }
    }
})

export const { updateCounterList, updateCounterValue } = counterSlice.actions

export default counterSlice.reducer
