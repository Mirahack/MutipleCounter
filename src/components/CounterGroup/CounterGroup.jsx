import Counter from "../Counter/Counter"
import { useSelector } from 'react-redux'

function CounterGroup() {

    const counterList = useSelector((state) => state.counterList)

    return (
        <div>
            {
                counterList.map((value, index) => <Counter index={index} value={value.value} ></Counter>)
            }
        </div>
        
    )

}

export default CounterGroup