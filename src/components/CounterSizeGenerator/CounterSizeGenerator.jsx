import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { updateCounterList } from '../../redux/counterSlice'

const CounterSizeGenerator = () => {

  const counterList = useSelector((state) => state.counterList)
  const dispatch = useDispatch()

    function changeSize(event) {
      dispatch(updateCounterList(Number(event.target.value)))
    }

  return (
    <div>
        size: <input type="number" onChange={changeSize} value={counterList.size}/>
    </div>
  )
}

export default CounterSizeGenerator