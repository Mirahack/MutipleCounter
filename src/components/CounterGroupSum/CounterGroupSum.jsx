import React from 'react'
import { useSelector } from 'react-redux'

const CounterGroupSum = () => {

  const counterList = useSelector((state) => state.counterList)

  const sum = counterList.reduce((count, counterObj) => count + counterObj.value, 0)

  return (
    <div>sum: <span>{sum}</span></div>
  )
}

export default CounterGroupSum