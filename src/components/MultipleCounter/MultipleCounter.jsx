import CounterGroup from '../CounterGroup/CounterGroup'
import CounterSizeGenerator from '../CounterSizeGenerator/CounterSizeGenerator'
import CounterGroupSum from '../CounterGroupSum/CounterGroupSum'

const MultipleCounter = () => {

    return (
        <div>
            <CounterSizeGenerator></CounterSizeGenerator>
            <CounterGroupSum></CounterGroupSum>
            <CounterGroup></CounterGroup>
        </div>
    )
}

export default MultipleCounter