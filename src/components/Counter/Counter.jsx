import React from "react"
import { updateCounterValue } from '../../redux/counterSlice'
import { useDispatch } from 'react-redux'

function Counter(props) {

    const dispatch = useDispatch()

    function increment() {
        dispatch(updateCounterValue({index: props.index, value: props.value + 1}))
    }

    function decrement() {
        dispatch(updateCounterValue({index: props.index, value: props.value - 1}))
    }

    return (
        <div id="counter">
            <button onClick={increment}>+</button>
            <span>&nbsp;&nbsp;{props.value}&nbsp;&nbsp;</span>
            <button onClick={decrement}>-</button>
        </div>
    )
}

export default Counter