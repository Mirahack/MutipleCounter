import './App.css';
import './components/Counter/Counter.css'
import MultipleCounter from './components/MultipleCounter/MultipleCounter';

function App() {
  return (
    <div className="App">
      <MultipleCounter></MultipleCounter>
    </div>
  );
}

export default App;
